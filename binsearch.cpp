#include <stdio.h>
#include <iostream>
#include <assert.h>
using namespace std;

int binsearch(int a[],int const e, int lo,int hi){
	while (lo < hi){
		int mi = (lo + hi) >> 1;
		if (e < a[mi]) hi = mi ;
		else lo = mi + 1;
	}
	lo--;
	return a[lo]==e?lo:-1;
}



int main(){
	int a[] = { 1, 2, 3, 6, 7, 7, 7, 8, 8, 9, 10 };
	int ans[] = { -1, 0, 1, 2, -1, -1, 3, 6, 8, 9, 10, -1 };
	for (int i = 0; i < 12; i++){
		assert(binsearch(a, i, 0, 11) == ans[i]);
	}
}



